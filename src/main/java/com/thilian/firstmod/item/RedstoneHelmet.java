package com.thilian.firstmod.item;

import static com.thilian.firstmod.TheMod.REDSTONE_ARMOR;

import net.minecraft.inventory.EntityEquipmentSlot;
import net.minecraft.item.ItemArmor;

public class RedstoneHelmet extends ItemArmor {

	public RedstoneHelmet() {
		super(REDSTONE_ARMOR, 4, EntityEquipmentSlot.HEAD);
		setUnlocalizedName("redstone_helmet");
		setRegistryName("redstone_helmet");
	}

}
