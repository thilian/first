package com.thilian.firstmod.item;

import static com.thilian.firstmod.TheMod.REDSTONE;

import net.minecraft.item.ItemHoe;

public class RedstoneHoe extends ItemHoe {

	public RedstoneHoe() {
		super(REDSTONE);
		setUnlocalizedName("redstone_hoe");
		setRegistryName("redstone_hoe");
	}

}
