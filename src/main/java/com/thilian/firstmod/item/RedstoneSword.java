package com.thilian.firstmod.item;

import static com.thilian.firstmod.TheMod.REDSTONE;

import net.minecraft.item.ItemSword;

public class RedstoneSword extends ItemSword {

	public RedstoneSword() {
		super(REDSTONE);
		setUnlocalizedName("redstone_sword");
		setRegistryName("redstone_sword");
	}

}
