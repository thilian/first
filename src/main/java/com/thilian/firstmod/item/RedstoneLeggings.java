package com.thilian.firstmod.item;

import static com.thilian.firstmod.TheMod.REDSTONE_ARMOR;

import net.minecraft.inventory.EntityEquipmentSlot;
import net.minecraft.item.ItemArmor;

public class RedstoneLeggings extends ItemArmor {

	public RedstoneLeggings() {
		super(REDSTONE_ARMOR, 4, EntityEquipmentSlot.LEGS);
		setUnlocalizedName("redstone_leggings");
		setRegistryName("redstone_leggings");
	}

}
