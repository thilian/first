package com.thilian.firstmod.item;

import static com.thilian.firstmod.TheMod.REDSTONE_ARMOR;

import net.minecraft.inventory.EntityEquipmentSlot;
import net.minecraft.item.ItemArmor;

public class RedstoneChestplate extends ItemArmor {

	public RedstoneChestplate() {
		super(REDSTONE_ARMOR, 4, EntityEquipmentSlot.CHEST);
		setUnlocalizedName("redstone_chestplate");
		setRegistryName("redstone_chestplate");
	}

}
