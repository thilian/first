package com.thilian.firstmod.item;

import static com.thilian.firstmod.TheMod.REDSTONE;

import net.minecraft.item.ItemSpade;

public class RedstoneShovel extends ItemSpade {

	public RedstoneShovel() {
		super(REDSTONE);
		setUnlocalizedName("redstone_shovel");
		setRegistryName("redstone_shovel");
	}

}
