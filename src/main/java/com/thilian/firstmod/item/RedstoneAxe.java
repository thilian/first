package com.thilian.firstmod.item;

import static com.thilian.firstmod.TheMod.REDSTONE;

import net.minecraft.item.ItemAxe;

public class RedstoneAxe extends ItemAxe {

	public RedstoneAxe() {
		super(REDSTONE, 10.0F, -3.0F);
		setUnlocalizedName("redstone_axe");
		setRegistryName("redstone_axe");
	}

}
