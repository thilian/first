package com.thilian.firstmod.item;

import static com.thilian.firstmod.TheMod.REDSTONE_ARMOR;

import net.minecraft.inventory.EntityEquipmentSlot;
import net.minecraft.item.ItemArmor;

public class RedstoneBoots extends ItemArmor {

	public RedstoneBoots() {
		super(REDSTONE_ARMOR, 4, EntityEquipmentSlot.FEET);
		setUnlocalizedName("redstone_boots");
		setRegistryName("redstone_boots");
	}

}
