package com.thilian.firstmod.item;

import static com.thilian.firstmod.TheMod.REDSTONE;

import net.minecraft.item.ItemPickaxe;

public class RedstonePickaxe extends ItemPickaxe {

	public RedstonePickaxe() {
		super(REDSTONE);
		setUnlocalizedName("redstone_pickaxe");
		setRegistryName("redstone_pickaxe");
	}

}
