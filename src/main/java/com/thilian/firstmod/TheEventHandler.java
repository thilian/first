package com.thilian.firstmod;

import com.thilian.firstmod.item.RedstoneAxe;
import com.thilian.firstmod.item.RedstoneBoots;
import com.thilian.firstmod.item.RedstoneChestplate;
import com.thilian.firstmod.item.RedstoneHelmet;
import com.thilian.firstmod.item.RedstoneHoe;
import com.thilian.firstmod.item.RedstoneLeggings;
import com.thilian.firstmod.item.RedstonePickaxe;
import com.thilian.firstmod.item.RedstoneShovel;
import com.thilian.firstmod.item.RedstoneSword;

import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.item.Item;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.registries.IForgeRegistry;

public class TheEventHandler {

	@SubscribeEvent
	public void registerItems(RegistryEvent.Register<Item> event) {
		IForgeRegistry<Item> registry = event.getRegistry();
		registerItems(registry, 
				new RedstoneAxe(), 
				new RedstoneHoe(),
				new RedstonePickaxe(), 
				new RedstoneShovel(), 
				new RedstoneSword(),
				new RedstoneBoots(),
				new RedstoneChestplate(),
				new RedstoneHelmet(),
				new RedstoneLeggings()
			);
	}

	private void registerItems(IForgeRegistry<Item> registry, Item... items) {
		for (Item item : items) {
			registerItem(registry, item);
		}
	}

	private void registerItem(IForgeRegistry<Item> registry, Item item) {
		registry.register(item);
		ModelLoader.setCustomModelResourceLocation(item, 0,
				new ModelResourceLocation(item.getRegistryName(), "inventory"));
	}

}
