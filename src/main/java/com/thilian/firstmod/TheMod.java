package com.thilian.firstmod;

import net.minecraft.init.SoundEvents;
import net.minecraft.item.Item.ToolMaterial;
import net.minecraft.item.ItemArmor.ArmorMaterial;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.common.util.EnumHelper;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;

@Mod(modid = TheMod.MODID, version = TheMod.VERSION)
public class TheMod {

	public static final String MODID = "firstmod";
	public static final String VERSION = "1.0";

	public static final ToolMaterial REDSTONE = EnumHelper.addToolMaterial("REDSTONE", 3, 1000, 10.0F, 6.0F, 30);
	public static final ArmorMaterial REDSTONE_ARMOR = EnumHelper.addArmorMaterial("REDSTONE_ARMOR", "firstmod:redstone", 7, new int[]{2, 5, 6, 2}, 25, SoundEvents.ITEM_ARMOR_EQUIP_GOLD, 0.0F);
	
	@EventHandler
	public void preInit(FMLPreInitializationEvent event) {
		MinecraftForge.EVENT_BUS.register(new TheEventHandler());

	}
	
}
